Voice Repeaters
===============

Voice repeaters are well covered in [freq-spec]_. All of the information
necessary to tune to a repeater can be included in the comment field of an
APRS object or item.

Repeaters are unlikely to be moving around, and so where callsigns are used as
the object name (preferred in localinfo) these are better transmitted as items
due to the shorter format for the packets.

The :class:`Repeater <localinfo.repeater.Repeater>` class is a subclass of
:class:`LocalEntity <localinfo.entity.LocalEntity>` and makes the same
attributes available.

Where an offset is greater than can be represented in the allowed 4 characters,
the full uplink frequency will be included.

.. autoclass:: localinfo.repeater.Repeater
   :members:
