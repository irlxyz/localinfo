APRS Local Information
======================

localinfo is a Python library and APRS client that can be used to generate
objects of relevance to amateurs in the local area. It builds on the `Local
Info Initiative <http://www.aprs.org/localinfo.html>`_ to generate APRS objects
in a standard format to allow radio amateurs to find each other and to make
contacts.

.. image:: _static/localinfo.png

It aims to bring greater visibility to:

* Voice repeaters (Analogue, Digital, Multi-mode, Parrot, etc.)
* Simplex gateways (EchoLink, D-STAR, DMR, etc.)
* Television or SSTV repeaters
* Beacons
* Packet nodes
* Clubs and Meetings
* Regular nets
* Satellites

As a secondary objective, this implementation will also inform the
implementation of `aprsd(8) <https://man.hambsd.org/aprsd.8>`_ to allow for
HamBSD digipeater and IGate operators to adopt objects that are in their local
area and to generate these themselves on RF.

The `Automatic Frequency Reporting System
<http://www.aprs.org/info/freqspec.txt>`_ forms the basis for many of the
object formats, however not all types of object are covered here and new
formats will be defined for those.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   objects
   repeaters
   etcc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
