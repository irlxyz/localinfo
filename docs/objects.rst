Base Objects
============

All positions, objects and items in an APRS net use :class:`LocalEntity
<localinfo.entity.LocalEntity>` or a subclass of LocalEntity.

.. autoclass:: localinfo.entity.LocalEntity
    :members:

Symbols
-------

The symbols to be used for entities are represented by an enumeration. This is
deliberately non-exhaustive to try to nudge towards consistency and deliberate
choices. Many parts of the APRS protocol rely on the symbol for the correct
decoding of the packet. Forcing the use of specific symbols with the local
information formats would be consistent with existing protocol restrictions.

.. autoclass:: localinfo.entity.APRSSymbol
    :members:

Data Extensions
---------------

.. automodule:: localinfo.extensions
   :members:
   :undoc-members:

Frequencies
-----------

.. automodule:: localinfo.freq
   :members:
   :undoc-members:
