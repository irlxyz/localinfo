from decimal import Decimal
from typing import Callable
from typing import Generator
from typing import Tuple

from nose.tools import assert_equal

from localinfo.freq import encode_ctcss
from localinfo.freq import encode_freq
from localinfo.freq import encode_offset
from localinfo.freq import encode_tone


def check_encode_freq(freq: Decimal, expected: str) -> None:
    assert_equal(encode_freq(freq), expected)


def test_encode_freq() -> Generator[Tuple[Callable[[Decimal, str], None],
                                          Decimal, str], None, None]:
    tests = [
        ("3", "003.000"),
        ("3.00000000000000000", "003.000"),
        ("3.520", "003.520"),
        ("29", "029.000"),
        ("50.2", "050.200"),
        ("145", "145.000"),
        ("145.55", "145.550"),
        ("1200", "A00.000"),
        ("1201", "A01.000"),
        ("1202.3", "A02.300"),
        # The following are the examples from freqspec.txt
        ("1296", "A96.000"),
        ("2320", "B20.000"),
        ("2401", "C01.000"),
        ("3401", "D01.000"),
        ("5651", "E51.000"),
        ("5760", "F60.000"),
        ("5830", "G30.000"),
        ("10101", "H01.000"),
        ("10201", "I01.000"),
        ("10368", "J68.000"),
        ("10401", "K01.000"),
        ("10501", "L01.000"),
        ("24048", "M48.000"),
        ("24101", "N01.000"),
        ("24201", "O01.000"),
    ]
    for test in tests:
        yield check_encode_freq, Decimal(test[0]), test[1]


def check_encode_offset(tx: Decimal, rx: Decimal, expected: str) -> None:
    assert_equal(encode_offset(tx, rx), expected)


def test_encode_offset() -> Generator[Tuple[Callable[
        [Decimal, Decimal, str], None], Decimal, Decimal, str], None, None]:
    tests = [
        ("50.84", "51.34", "+050"),
        ("145.775", "145.175", "-060"),
        ("145.775000", "145.175", "-060"),
        ("433.1", "434.7000", "+160"),
        ("430.95", "438.55", "+760"),
        ("1297", "1291", "-600"),
    ]
    for test in tests:
        yield check_encode_offset, Decimal(test[0]), Decimal(test[1]), test[2]


def check_encode_ctcss(freq: Decimal, narrow: bool, expected: str) -> None:
    assert_equal(encode_ctcss(freq, narrow), expected)


def test_encode_ctcss() -> Generator[Tuple[Callable[
        [Decimal, bool, str], None], Decimal, bool, str], None, None]:
    tests = [
        ("67.0", True, "c067"),
        ("67.0", False, "C067"),
        ("88.5", False, "C088"),
        ("110.9", False, "C110"),
        ("110.900", False, "C110"),
    ]
    for test in tests:
        yield check_encode_ctcss, Decimal(test[0]), test[1], test[2]


def check_encode_tone(freq: Decimal, narrow: bool, expected: str) -> None:
    assert_equal(encode_tone(freq, narrow), expected)


def test_encode_tone() -> Generator[Tuple[Callable[[Decimal, bool, str], None],
                                          Decimal, bool, str], None, None]:
    tests = [
        ("67.0", True, "t067"),
        ("67.0", False, "T067"),
        ("88.5", False, "T088"),
        ("110.9", False, "T110"),
        ("110.900", False, "T110"),
    ]
    for test in tests:
        yield check_encode_tone, Decimal(test[0]), test[1], test[2]


class LocalEntity:
    pass
