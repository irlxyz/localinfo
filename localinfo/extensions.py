import math
from dataclasses import dataclass
from dataclasses import field


class DataExtension:
    pass


@dataclass
class CourseSpeed(DataExtension):
    """Course and Speed (CSE/SPD) data extension for APRS reports."""

    course: int
    """Direction of travel as a bearing in degrees.
    For an unknown bearing, use None.
    """
    speed: int
    """Speed of travel in kilometers/hour."""
    def __str__(self) -> str:
        """Return data extension as a string.

        .. note::

            The wire format for this data extension specifies the speed in
            knots. This will be converted automatically.

        """
        if self.course is None:
            cse = 0
        elif self.course == 0:
            cse = 360
        else:
            cse = self.course
        if self.speed is None:
            spd = 0
        else:
            spd = self.speed / 1.852
        return f"{cse:03d}/{spd:03.0f}"

    @classmethod
    def from_content(cls, content: str):
        """Create a new CourseSpeed from packet content."""
        if len(content) != 7 or content[3] != "/":
            return ValueError("Could not parse content. Expected nnn/nnn but "
                              f"got: {content}")
        if content[:3] in ["000", "   ", "..."]:
            cse = None
        elif content[:3] == "360":
            cse = 0
        else:
            cse = int(content[:3])
        if content[4:] in ["000", "   ", "..."]:
            spd = None
        else:
            spd = int(content[4:]) * 1.852

        return cls(cse, spd)


@dataclass
class PreCalculatedRange(DataExtension):
    """Pre-Calculated Radio Range (RNG) data extension for APRS reports."""

    rng: int
    """Pre-calculated radio range in kilometers (km)."""
    def __str__(self) -> str:
        """Return data extension as a string.

        .. note::

            The wire format for this data extension specifies the range in
            miles. This will be converted automatically.

        """
        return f"RNG{self.rng / 1.609:04.0f}"

    @classmethod
    def from_content(cls, content: str):  # -> PreCalculatedRange
        """Create a new PreCalculatedRange from packet content."""
        if len(content) != 7 or content[0:3].upper() != "RNG":
            return ValueError("Could not parse content. Expected RNGnnnn but "
                              f"got: {content}")
        return cls(int(content[3:8]) * 1.609)


@dataclass
class PowerHeightGain(DataExtension):
    """Power Height Gain (PHG) data extension for APRS reports."""

    power: int
    """Transmitter power output in Watts (W)."""
    height: int
    """Height above average terrain (not above sea level) in meters."""
    gain: int
    """Gain of the antenna in decibels (dB)."""
    directivity: int = field(default=None)
    """Direction of the antenna as a bearing in degrees.
    For an omnidirectional antenna, use None.
    """
    def range(self) -> float:
        """Calculate the radio range given the power, height and gain values.

        The calculation is based on the one used in [aprs-spec]_ for range
        circle plots. The returned value is in kilometers (km).
        """
        power_ratio = 10**(self.gain / 10)
        return 4.122 * math.sqrt(self.height * math.sqrt(
            (self.power / 10.0) * (power_ratio / 2.0)))

    def precalculate(self) -> PreCalculatedRange:
        """Return a pre-calculated range data extension based on this one."""
        return PreCalculatedRange(self.range())

    def __str__(self) -> str:
        """Return data extension as a string."""
        power = round(math.sqrt(self.power))
        haat = round(math.log2(self.height * 0.328))
        if self.directivity:
            directivity = round(self.directivity / 45.0)
        else:
            directivity = "0"
        return f"PHG{power}{haat}{self.gain}{directivity}"

    @classmethod
    def from_content(cls, content: str):  # -> PowerHeightGain
        """Create a new PowerHeightGain from packet content."""
        if len(content) != 7 or content[0:3].upper() != "PHG":
            return ValueError("Could not parse content. Expected PHGnnnn but "
                              f"got: {content}.")
        power = int(content[3])**2
        height = 3.048 * 2**int(content[4])
        gain = int(content[5])
        if content[6] == "0":
            directivity = None
        else:
            directivity = int(content[6]) * 45
            if directivity > 360:
                raise ValueError("Incorrect value for directivity. Expected "
                                 "value between 0 and 8 but found: "
                                 f"{content[6]}.")
        return cls(power, height, gain, directivity)
