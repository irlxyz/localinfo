from decimal import Decimal

from localinfo.entity import APRSSymbol
from localinfo.entity import LocalEntity
from localinfo.freq import OffsetOutOfRangeError
from localinfo.freq import encode_freq
from localinfo.freq import encode_offset
from localinfo.freq import encode_tone


class Repeater(LocalEntity):
    freq_tx: Decimal
    """The transmit frequency of the repeater in MHz."""
    freq_rx: Decimal
    """The receive frequency of the repeater in MHz."""
    tone: Decimal
    """The frequency of the necessary CTCSS tone in Hz."""
    narrow: bool
    """Is the repeater channel narrowband?"""
    description: str

    def __init__(self, name: str, freq_tx: Decimal, freq_rx: Decimal,
                 tone: Decimal, narrow: bool, latitude: Decimal,
                 longitude: Decimal, description: str):
        """Create a new repeater object."""
        super().__init__(name, APRSSymbol.REPEATER, latitude, longitude,
                         None, description)
        self.freq_tx = freq_tx
        self.freq_rx = freq_rx
        self.tone = tone
        self.narrow = narrow

    def localinfo(self) -> str:
        """Return local information comment field for repeater object.

        This will encode the frequency, tone, offset and description into the
        APRS frequency specification format.

        .. testsetup::

            from decimal import Decimal
            from localinfo.repeater import Repeater

        >>> Repeater("GB3GN", Decimal("145.7750"), Decimal("145.1750"),
        ... Decimal("67.0"), True, Decimal("57.017715"), Decimal("-2.359003"),
        ... "Banchory").localinfo()
        '145.775MHz t067 -060 Banchory'

        """
        result = f"{encode_freq(self.freq_tx)}MHz "
        try:
            offset = encode_offset(self.freq_tx, self.freq_rx)
        except OffsetOutOfRangeError:
            offset = None
            result += f"{encode_freq(self.freq_rx)}rx "
        if self.tone:
            result += f"{encode_tone(self.tone, self.narrow)} "
        if offset:
            result += f"{encode_offset(self.freq_tx, self.freq_rx)} "
        result += f"{self.description}"
        return result

    def __repr__(self) -> str:
        """Return representation of the repeater object."""
        return f"<Repeater {self.name} {self.localinfo()}>"
