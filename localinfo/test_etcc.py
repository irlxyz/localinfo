from decimal import Decimal

from nose.tools import assert_equal

from localinfo.etcc import RepeaterSatNavFile


def test_repeater_satnav_len():
    """Test that all the repeaters have been read from the example file.

    .. note::

        GB3TD is listed in the file twice. Additionally, GB3OA and GB3DB have
        invalid locations. There are 590 lines in the example file, of which
        these 3 are expected to be missing, and so the expected total is 587.

    """
    satnav = RepeaterSatNavFile.from_file("examples/satnav.csv")
    assert_equal(len(satnav._locations), 587)


def test_repeater_satnav():
    satnav = RepeaterSatNavFile.from_file("examples/satnav.csv")
    tests = [
        ("GB3GN", (Decimal("57.017715"), Decimal("-2.359003"))),
        ("GB3BB", (Decimal("51.97"), Decimal("-3.47"))),
    ]
    for test in tests:
        assert_equal(satnav.lookup(test[0]), test[1])
