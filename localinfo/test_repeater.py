from decimal import Decimal

from nose.tools import assert_equal

from localinfo.repeater import Repeater


def check_repeater_localinfo(test) -> None:
    r = Repeater(*test[0])
    assert_equal(r.localinfo(), test[1])


def test_repeater_localinfo():
    tests = [
        (("GB3GN", Decimal("145.775"),
          Decimal("145.175"), Decimal("67.0"), True, Decimal("57.017715"),
          Decimal("-2.359003"), "Banchory"), "145.775MHz t067 -060 Banchory"),
        (("G1EFU", Decimal("430.9625"),
          Decimal("438.5625"), Decimal("82.5"), True, Decimal("53.48"),
          Decimal("-2.58"), "Wigan"), "430.962MHz t082 +760 Wigan"),
        (("GB3AK", Decimal("1297.3500"), Decimal("1277.3500"), Decimal("94.8"),
          True, Decimal("51.591373"), Decimal("-2.539454"), "Bristol"),
         "A97.350MHz A77.350rx t094 Bristol"),
    ]
    for test in tests:
        yield check_repeater_localinfo, test
