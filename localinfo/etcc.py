import csv
import decimal
from io import StringIO
from typing import Any
from typing import Dict
from typing import List
from typing import TextIO
from typing import Tuple

import requests

from localinfo.repeater import Repeater


class ETCCFile:
    """An abstract file from the ETCC."""

    @classmethod
    def from_file(cls, path: str):
        """Create a new file using a filesystem source.

        :param path: The filesystem path of the file to use.
        """
        with open(path) as source:
            return cls(source)

    @classmethod
    def from_web(cls, *, url: str = None):
        """Create a new file using the latest file from the web.

        If no URL is provided, the default URL for the class will be used. The
        default URL will be found in the DOWNLOAD_URL attribute of the class.

        :param url: The URL to download the file from.
        """
        if url is None:
            url = cls.DOWNLOAD_URL
        r = requests.get(url)
        return cls(StringIO(r.content.decode("utf-8")))


class RepeaterSatNavFile(ETCCFile):
    """A SatNav file as downloaded from the UK Repeater website.

    The ETCC UK Repeater website makes available `SatNav files
    <https://ukrepeater.net/satnav.htm>`_ to add repeaters as points of
    interest to SatNav devices. This class allows for these files to be used to
    provide location information for repeaters.

    This will usually be used either from a file in the local filesystem, or
    downloading the CSV from the website directly.

    .. testsetup::

        from localinfo.etcc import RepeaterSatNavFile

    >>> RepeaterSatNavFile.from_file("../examples/satnav.csv")
    ... #  doctest: +ELLIPSIS
    <RepeaterSatNavFile ...>
    >>> RepeaterSatNavFile.from_web() #  doctest: +SKIP
    <RepeaterSatNavFile ...>

    :param source: A TextIO source for the CSV file content.
    """

    _locations: Dict[str, Tuple[decimal.Decimal, decimal.Decimal]]

    DOWNLOAD_URL = "https://ukrepeater.net/satnav1.php"
    """The direct download URL for this file from the UK Repeater website."""

    def __init__(self, source: TextIO):
        """Create a new SatNav file initialised with a CSV file."""
        self._locations = dict()
        for row in csv.reader(source):
            call = row[2].strip("'")
            try:
                if "--" in row[0]:
                    row = (row[0].replace("--", "-"), row[1])
                self._locations[call] = (
                    decimal.Decimal(row[1]),
                    decimal.Decimal(row[0]))
            except decimal.InvalidOperation:
                pass

    def lookup(self, call: str) -> Tuple[decimal.Decimal, decimal.Decimal]:
        """Return the location of the repeater given a callsign.

        The position will be returned as a tuple containing the latitude and
        longitude. If no callsign is found, a tuple is still returned with
        both latitude and longitude set to None.

        :param call: The callsign to lookup.
        """
        return self._locations.get(call, (None, None))

    def __repr__(self):
        """Return a representation of the SatNav file.

        The callsigns for all known repeaters will be included in the
        representation.
        """
        return "<RepeaterSatNavFile " + ",".join(
            self._locations.keys()) + ">"


class RepeatersWithStatusCSVFile(ETCCFile):
    """A repeaters with status file as downloaded from the UK Repeater website.

    The ETCC UK Repeater website makes available `CSV files
    <https://ukrepeater.net/csvfiles.htm>`_ containing all voice repeaters
    along with their current status.

    This will usually be used either from a file in the local filesystem, or
    downloading the CSV from the website directly.

    .. testsetup::

        from localinfo.etcc import RepeatersWithStatusCSVFile

    >>> RepeatersWithStatusCSVFile.from_file("../examples/repeaters.csv")
    ... #  doctest: +ELLIPSIS
    <RepeatersWithStatusCSVFile ...>
    >>> RepeatersWithStatusCSVFile.from_web() #  doctest: +SKIP
    <RepeatersWithStatusCSVFile ...>

    :param source: A TextIO source for the CSV file content.
    """

    _repeaters: List[Dict[str, Any]]

    DOWNLOAD_URL = "https://ukrepeater.net/csvcreatewithstatus.php"
    """The direct download URL for this file from the UK Repeater website."""

    def __init__(self, source: TextIO):
        """Create a new repeaters status file initialised with a CSV file."""
        self._repeaters = []
        for row in csv.DictReader(source):
            self._repeaters.append(row)

    def repeaters(self,
                  operational_only: bool = True,
                  satnav: RepeaterSatNavFile = None):
        result = []
        for repeater in self._repeaters:
            if repeater["mode"] != "AV":
                continue
            if "-" in repeater["repeater"]:
                continue
            if operational_only and repeater["status"] != "OPERATIONAL":
                continue
            call = repeater["repeater"]
            freq_tx = decimal.Decimal(repeater["TX"])
            freq_rx = decimal.Decimal(repeater["RX"])
            try:
                tone = decimal.Decimal(repeater["code"])
            except decimal.InvalidOperation:
                tone = None
            if satnav:
                latitude, longitude = satnav.lookup(call)
            else:
                latitude = longitude = None
            description = repeater["where"].title()
            result.append(
                Repeater(call, freq_tx, freq_rx, tone, True, latitude,
                         longitude, description))
        return result

    def __repr__(self):
        """Return a representation of the repeaters CSV file.

        The callsigns for all known repeaters will be included in the
        representation.
        """
        return "<RepeatersWithStatusCSVFile " + ",".join(
            [x['repeater'] for x in self._repeaters]) + ">"
