from decimal import Decimal

microwave_table = {
    1200: 'A',
    2300: 'B',
    2400: 'C',
    3400: 'D',
    5600: 'E',
    5700: 'F',
    5800: 'G',
    10100: 'H',
    10200: 'I',
    10300: 'J',
    10400: 'K',
    10500: 'L',
    24000: 'M',
    24100: 'N',
    24200: 'O',
}


def encode_freq(freq: Decimal) -> str:
    """Return a frequency encoded for use in a local information comment text.

    Frequencies are encoded with 3 digits before the decimal point and 3 digits
    after the decimal point. Where required, this is padded with zero digits.

    No "MHz" is appended to the frequency as the same function may also be used
    for cross band repeaters, or those with non-standard offsets, to provide
    an explicit receive frequency.

    .. testsetup::

        from localinfo.freq import encode_freq
        from decimal import Decimal

    >>> encode_freq(Decimal("51.51"))
    '051.510'
    >>> encode_freq(Decimal("145.5"))
    '145.500'

    """
    freq = Decimal(freq)
    result = None
    if freq < 1000:
        result = f"{freq:07.3f}"
    for microwave in microwave_table:
        if freq >= microwave and freq < microwave + 100:
            result = f"{microwave_table[microwave]}{(freq - microwave):06.3f}"
            break
    if result:
        if "." not in result:
            result += "."
        return result.ljust(6, "0")
    raise FrequencyOutOfRangeError


def encode_offset(tx: Decimal, rx: Decimal) -> str:
    """Return an offset for use in a local information comment text.

    Offsets are encoded as a sign, plus (+) or minus (-), followed by 3 digits.
    The digits show the value of the offset in maHz (10⁴). The digits are
    right aligned with leading zero digits if necessary to pad.

    .. testsetup::

        from localinfo.freq import encode_offset
        from decimal import Decimal

    >>> encode_offset(Decimal("145.775"), Decimal("145.175"))
    '-060'

    """
    tx = Decimal(tx)
    rx = Decimal(rx)
    off = ((rx - tx) * 100) // 1
    if abs(off) >= 1000:
        raise OffsetOutOfRangeError
    if off < 0:
        return f"-{abs(off):03}"
    if off > 0:
        return f"+{off:03}"
    if off == 0:
        return "-000"


def encode_ctcss(ctcss: Decimal, narrow: bool = False) -> str:
    """Return an CTCSS tone for use in a local information comment text.

    Tones are encoded as 3 digits in Hz, with the fractional part truncated.
    The digits are right aligned with leading zero digits if necessary to pad.
    CTCSS tones will be prefixes with either an uppercase "C", if this is
    entity is using a wide channel, or a lowercase "c", if this entity is using
    a narrow channel.

    .. testsetup::

        from localinfo.freq import encode_ctcss
        from decimal import Decimal

    >>> encode_ctcss(Decimal("67.0"))
    'C067'
    >>> encode_ctcss(Decimal("103.5"), True)
    'c103'

    """
    if ctcss < 0 or ctcss >= 1000:
        raise ToneOutOfRangeError
    c = "c" if narrow else "C"
    ctcss = ctcss // 1
    return f"{c}{ctcss:03f}"


def encode_tone(tone: Decimal, narrow: bool = False) -> str:
    """Return an tone for use in a local information comment text.

    Tones are encoded as 3 digits in Hz, with the fractional part truncated.
    The digits are right aligned with leading zero digits if necessary to pad.
    Tones will be prefixes with either an uppercase "T", if this is
    entity is using a wide channel, or a lowercase "t", if this entity is using
    a narrow channel.

    .. testsetup::

        from localinfo.freq import encode_tone
        from decimal import Decimal

    >>> encode_tone(Decimal("67.0"))
    'T067'
    >>> encode_tone(Decimal("103.5"), True)
    't103'

    """
    if tone < 0 or tone >= 1000:
        raise ToneOutOfRangeError
    t = "t" if narrow else "T"
    tone = tone // 1
    return f"{t}{tone:03f}"


class FrequencyOutOfRangeError(ValueError):
    pass


class OffsetOutOfRangeError(ValueError):
    pass


class ToneOutOfRangeError(ValueError):
    pass
