from decimal import Decimal
from enum import Enum

from localinfo.extensions import DataExtension


class APRSSymbol(Enum):
    """An enumeration for APRS symbols."""

    table: str
    """Symbol table or overlay."""
    code: str
    """Symbol code."""
    def __init__(self, table, code):
        """Create a new APRSSymbol."""
        self.table = table
        self.code = code

    CLUB = ("C", "-")
    HOUSE = ("/", "-")
    LOGGEDON = ("/", "L")
    REPEATER = ("/", "r")


class LocalEntity:
    """An abstract entity in an APRS net.

    This entity could be encoded as a position report, object report or item
    report. Until encoded it is all of these, and none.

    :param name: Name of the entity. If intended to be used for a position
                 report this must be a valid AX.25 address.
    :param symbol: APRS symbol for the entity.
    :param latitude: Latitude in decimal degrees.
    :param longitude: Longitude in decimal degrees.
    :param description: Textual description of the entity.
    """

    name: str
    """Name of the entity."""
    symbol: APRSSymbol
    """APRS symbol for the entity."""
    latitude: Decimal
    """Latitude in decimal degrees."""
    longitude: Decimal
    """Longitude in decimal degrees."""
    extension: DataExtension
    """APRS data extension."""
    description: str
    """Textual description of the entity."""
    def __init__(self,
                 name: str,
                 symbol: APRSSymbol,
                 latitude: Decimal,
                 longitude: Decimal,
                 extension: DataExtension,
                 description: str = ""):
        """Create a new LocalEntity."""
        self.name = name
        self.symbol = symbol
        self.latitude = latitude
        self.longitude = longitude
        self.extension = extension
        self.description = description
